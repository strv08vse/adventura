
Autor: V�t St��tesk�

Verze: LS 2017/2018

====================Popis programu==============================

Na z�klad� projektu Adventura, vytvo�en�m Ing. Janem ��hou pro kurz
Programov�n� v Jav� (4IT101), jsem vytvo�il jednoduchou textovou 
adventuru. Hra se zakl�d� na z�d�v�n� p��kaz� a postupn�m odhalov�n�m 
podivuhodn�ch okolnost� vra�dy.
Podrobn�j�� popis zp�sobu hran� lze nal�zt v u�ivatelsk� p��ru�cce.