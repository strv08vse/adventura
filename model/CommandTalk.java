package model;



public class CommandTalk implements ICommand
{
    private static final String NAME = "mluv";
    private GamePlan plan;

    /**
     *  Konstruktor tridy PrikazMluv
     */
    public CommandTalk(GamePlan plan)
    {
        this.plan = plan;
    }


    /**
     *Vykonává příkaz "mluv". Zkouší se hovořit se zadanou postavou. 
     *@param parametr - jmeno postavy, se kterou se ma komunikovat
     *@return fraze, ktorá se vypíše hráčovi
     */ 
    public String process(String... parameters) { 
        if (parameters.length == 0) {
            return "S kym bych si mel promluvit?";
        }
        String jmeno = parameters[0];
        Location curLocation = plan.getCurrentLocation();
        Character character = curLocation.containsCharacter(jmeno);
        if (character == null) {
            return "Nikdo takovy tu neni.";
        }
        else  {
            return character.getSpeech();
        }
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    public String getName() {
        return NAME;
    }
}
