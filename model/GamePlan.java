/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package model;

/**
 * Class HerniPlan - třída představující mapu a stav adventury.
 * 
 * Tato třída inicializuje prvky ze kterých se hra skládá:
 * vytváří všechny lokace, propojuje je vzájemně pomocí východů 
 * a pamatuje si aktuální lokaci, ve které se hráč právě nachází.
 *
 * @author     Michael Kolling, Luboš Pavlíček, Jarmila Pavlíčková, Jan Říha
 * @version    LS 2017/2018
 */
public class GamePlan {
    private static final String FINAL_LOCATION = "dalsi_pripad";
    
    private Inventory inventory;

    private Location currentLocation;

    /**
     * Konstruktor který vytváří jednotlivé lokace a propojuje je pomocí východů.
     */
    public GamePlan(Inventory i) {
        inventory = i;
        prepareWorldMap();
    }

    /**
     * Vytváří jednotlivé lokace a propojuje je pomocí východů.
     * Jako výchozí aktuální lokaci nastaví domeček.
     */
    private void prepareWorldMap() {
        // vytvářejí se jednotlivé lokace
        Location merlinuvPok = new Location("merlinuv_domecek","merlinuv domecek, zde bylo nalezeno merlinovo mrtve telo");
        Location vyreseni = new Location(FINAL_LOCATION, "a jaky pripad bude muset vyresit detektiv pikachu priste?");
        Location hostinec = new Location("hostinec","pravy a nefalsovany pajzl");
        Location nadvori = new Location("nadvori","nadvori královského hradu, nezvykle liduprazdne");
        Location mestecko = new Location("mestecko","mestecko, spoustu lidí, malo poridku");
        Location audi = new Location("audiencni_mistnost","audiencni mistnost, zde kral prijima sve hosty a poddane");
        Location soukro = new Location("soukrome_komnaty","kralovy soukrome komnaty, zde si kral uziva volneho casu");
        Location trznice = new Location("trznice","trznice, lidi sem chodí nakupovat vsemozne i nemozne veci");
        Location kuchyne = new Location("kuchyne","kuchyne, tady se vari jidlo pro cely hrad");
        Location tajem = new Location("tajemstvi","tajemstvi, jsme něčemu nastope");
        Location tajem2 = new Location("tajemstvi2","tajemstvi, už něco tusim... rychle nezbyva moc casu");
        Location tajem3 = new Location("tajemstvi3","bingo!");
        
   

        // přiřazují se průchody mezi lokacemi (sousedící lokace)
        merlinuvPok.addExit(nadvori);
        nadvori.addExit(merlinuvPok);
        nadvori.addExit(audi);
        nadvori.addExit(mestecko);
        mestecko.addExit(nadvori);
        mestecko.addExit(hostinec);
        mestecko.addExit(trznice);
        mestecko.addExit(tajem);
        hostinec.addExit(mestecko);
        trznice.addExit(mestecko);
        audi.addExit(soukro);
        audi.addExit(kuchyne);
        audi.addExit(nadvori);
        soukro.addExit(audi);
        kuchyne.addExit(audi);
        tajem.addExit(mestecko);
        tajem.addExit(tajem2);
        tajem2.addExit(tajem);
        tajem2.addExit(tajem3);
        tajem3.addExit(tajem2);
        tajem3.addExit(vyreseni);

        // hra začíná v domečku
        currentLocation = merlinuvPok;

        // vytvářejí se předměty
        Item stul = new Item("stul", "Obrovsky a tezky dubovy stul", false);
        
        Item rum = new Item("rum", "Lahev kvalitniho vyzraleho rumu");

        Item klobouk = new Item("klobouk_kouzelnika_Pokustona","něco");
        Item hul = new Item("tajemna_hulka","hmmm tajmna");
        Item Xray = new Item("x-ray_bryle","co s nimi asi meslin delal");
        Item kniha1 = new Item("kniha_podivnosti","podivnost");
        Item kniha2 = new Item("kniha","co se cte");
        Item postel = new Item("postel","vec na spani", false);
        Item excalibur = new Item("Excalibur","kraluv mec", false);
        Item socha = new Item("socha","velka", false);
        Item kun = new Item("kun","neni zavodni", false);
        Item kamen = new Item("kamen","kamen");
        Item plechovka = new Item("plechovka","jak se tu vzala?");
        Item trun = new Item("trun","presne pro kralovske pozadi", false);
        Item gobelin = new Item("gobelin","na ozdobu", false);
        Item nechtel = new Item("vec_co_jsem_najit_nechtel","bleee");
        Item bordel1 = new Item("bordel1","fuj");
        Item bordel2 = new Item("bordel2","fujfuj");
        Item kdoco = new Item("kdo_vi_co","co?co?");
        Item noviny = new Item("vcerejsi_noviny","blablabla");
        Item pstruh = new Item("pstruh","ryba");
        Item vajicka = new Item("vajicka","slepici");
        Item gulas = new Item("gulas","dobry a vydatny");
        Item vycep = new Item("vycep","dobre pivko", false);
        Item brejle = new Item("brejle","koho asi jsou?");
        Item cibule = new Item("cibule",";(");
        Item rajce = new Item("rajce","kecup");
        Item nuz = new Item("nuz","vypady jako od kecupu");
        Item amulet = new Item("TEN_kouzelny_amulet","ano opravdu je to on!");
        
        
        
        
        
        // vytvářejí se postavy
        Character batman = new Character("Batman","BATMAN: jsem batman!\n" + "PIKACHU: *Pikachuuu!");
        Character alucard = new Character("Alucard","PIKACHU: *pika *pika\n" + "ALUCARD: jen at prijdou, aspon bude zabava;)");
        Character tyrion = new Character("Tyrion Lannister","PIKACHU: *pika?\n" + "TYRION: *&#*-*/#*& bleee, dalsi runda na me!");
        Character vocko = new Character("Vocko_Szyslak","VOCKO: dobrej, cim muzu poslouzit?\n" + "PIKACHU: *pika *pika?\n" + "VOČKO: joo, vcera tady popijel nejakej divnej borec a nechal tady tyhle brejle");
        Character sherlock = new Character("Sherlock_Holmes","SHERLOCK: diky boze, ze uz jsi tu... zacinali jsme byt naprosto bezradni\n" + "PIKACHU: *pika *pika\n" + "SHERLOCK: cokoliv co jen budes potrebovat, jiz jsem ti zaridil kralovské povoleni na pristup kamkoliv kam jen bubes potrebovat");
        Character conan = new Character("Shinichi_Kudo","SHINICHI: cau, zjistil jsem, ze bude nutne prozkoumat kouzelnou knihu... mela by byt u merlina doma\n" + "PIKACHU: *pika");
        Character ryuk = new Character("Ryuk","RYUK: vedel jsem, ze to zvládnes! ted jen rekni kdo to byl a ja se o to postaram\n" + "PIKACHU: *pika *pika\n" + "RYUK: pisu si");
        Character deteL = new Character("L","L: nazdar, myslim, ze by jste meli najit nuz... slysel jsem, ze se neco takoveho nachazi v kuchyni");
        Character yuki = new Character("Yukihira_Souma","YUKIHIRA: dobrej detektive, dal byste si neco k jidlu?\n" + "PIKACHU: *Pikachuuu!\n" + "YUKIHIRA: tak sand nekdy priste");
        Character prodavac = new Character("prodavac","PIKACHU: *pika *pika\n" + "PRODAVAC: bohuzel ne:(");
        Character strazny = new Character("strazny","PIKACHU: *pika *pika\n" + "STRAZNY: bohuzel ne:/");
        
        // vkládají se předměty do jednotlivých lokací
        merlinuvPok.addItem(stul);
        hostinec.addItem(rum);
        merlinuvPok.addItem(klobouk);
        merlinuvPok.addItem(hul);
        merlinuvPok.addItem(Xray);
        merlinuvPok.addItem(kniha1);
        merlinuvPok.addItem(postel);
        mestecko.addItem(bordel1);
        mestecko.addItem(bordel2);
        mestecko.addItem(kdoco);
        mestecko.addItem(noviny);
        trznice.addItem(pstruh);
        trznice.addItem(vajicka);
        trznice.addItem(kniha2);
        hostinec.addItem(vycep);
        hostinec.addItem(gulas);
        hostinec.addItem(stul);
        hostinec.addItem(brejle);
        nadvori.addItem(socha);
        nadvori.addItem(kun);
        nadvori.addItem(kamen);
        nadvori.addItem(plechovka);
        audi.addItem(trun);
        audi.addItem(gobelin);
        audi.addItem(excalibur);
        soukro.addItem(postel);
        soukro.addItem(nechtel);
        kuchyne.addItem(cibule);
        kuchyne.addItem(rajce);
        kuchyne.addItem(nuz);
        tajem3.addItem(amulet);
        
        
        
        
        // vkládají se postavy do jednotlivých lokací
        merlinuvPok.addCharacter(sherlock);
        hostinec.addCharacter(vocko);
        hostinec.addCharacter(tyrion);
        mestecko.addCharacter(alucard);
        trznice.addCharacter(batman);
        trznice.addCharacter(prodavac);
        trznice.addCharacter(deteL);
        audi.addCharacter(strazny);
        kuchyne.addCharacter(yuki);
        tajem3.addCharacter(ryuk);
        soukro.addCharacter(conan);
        
        // zamykají se lokace
        tajem.setLocked(true);
        tajem2.setLocked(true);
        tajem3.setLocked(true);
        
        // přiřazují se klíče
        tajem.setKey(kniha1);
        tajem3.setKey(brejle);
        tajem2.setKey(nuz);
    }

    /**
     * Metoda vrací odkaz na aktuální lokaci, ve které se hráč právě nachází.
     *
     * @return    aktuální lokace
     */
    public Location getCurrentLocation() {
        return currentLocation;
    }

    /**
     * Metoda nastaví aktuální lokaci, používá se nejčastěji při přechodu mezi lokacemi
     *
     * @param    location nová aktuální lokace
     */
    public void setCurrentLocation(Location location) {
       currentLocation = location;
    }
    
    public Inventory getInventory() {
        return inventory;
    }
    
    public boolean isVictorious() {
        return currentLocation.getName().equals(FINAL_LOCATION);
    }

}
