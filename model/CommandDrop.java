package model;


/**
 * Třída CommandDrop představuje příkaz pro odhození
 * předmětu.
 *
 * @author     Vít Stříteský
 * @version    LS 2017/2018
 */
public class CommandDrop implements ICommand
{
    private static final String NAME = "odhod";
    private GamePlan plan;
    private Inventory inventory;

    /**
     *  Konstruktor třídy CommandDrop
     *  
     *  @param plan herní plán, ve kterém se bude ve hře "chodit"
     */    
    public CommandDrop(GamePlan plan) {
        this.plan = plan;
        this.inventory = plan.getInventory();
    }

    /**
     *  Provádí příkaz "odhod". Zkouší odhodit věc z batohu. Pokud v batohu není vypíše chybovou hlášku.
     *  Jinak věc odhodí do aktuálního prostoru.
     *
     * @param parametrs jméno odhazované věci
     * @return text informující o výsledku zpracování
     */ 
    public String process(String... parameters) {
        if (parameters.length == 0) {
            return "Nevim, co mam odhodit. Musis zadat nazev veci!\n";
        }

        String name = parameters[0];
        Location currentLocation = plan.getCurrentLocation();
        Item item = inventory.getItem(name);

        if (item == null) {
            return "Takova vec v batohu neni\n";            
        }
        else {
            plan.getInventory().dropItem(name);
            currentLocation.addItem(item);
            return "Vec " + name + " byla odhozena.\n";
        }
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    
    public String getName() {
        return NAME;
    }
    
}
