package model;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

/*******************************************************************************
 * Testovací třída HraTest slouží ke komplexnímu otestování
 * třídy Hra.
 *
 * @author     Jarmila Pavlíčková, Jan Říha
 * @version    LS 2017/2018
 */
public class GameTest {
    private Game game;

    /***************************************************************************
     * Metoda se provede před spuštěním každé testovací metody. Používá se
     * k vytvoření tzv. přípravku (fixture), což jsou datové atributy (objekty),
     * s nimiž budou testovací metody pracovat.
     */
    @Before
    public void setUp() {
        game = new Game();
    }

    /***************************************************************************
     * Testuje průběh hry, po zavolání každěho příkazu testuje, zda hra končí
     * a v jaké aktuální místnosti se hráč nachází.
     * Při dalším rozšiřování hry doporučujeme testovat i jaké věci nebo osoby
     * jsou v místnosti a jaké věci jsou v batohu hráče.
     */
    @Test
    public void testPlayerWin() {
        assertEquals("merlinuv_domecek", game.getGamePlan().getCurrentLocation().getName());
        game.processCommand("seber kniha_podivnosti");
        assertEquals(false, game.isGameOver());
        game.processCommand("jdi nadvori");
        assertEquals("nadvori", game.getGamePlan().getCurrentLocation().getName());
        assertEquals(false, game.isGameOver());
        game.processCommand("jdi audiencni_mistnost");
        assertEquals("audienční_místnost", game.getGamePlan().getCurrentLocation().getName());
        assertEquals(false, game.isGameOver());
        game.processCommand("jdi kuchyne");
        assertEquals("kuchyně", game.getGamePlan().getCurrentLocation().getName());
        assertEquals(false, game.isGameOver());
        game.processCommand("seber nuz");
        assertEquals(false, game.isGameOver());
        game.processCommand("jdi audiencni_mistnost");
        assertEquals("audienční_místnost", game.getGamePlan().getCurrentLocation().getName());
        assertEquals(false, game.isGameOver());
        game.processCommand("jdi nadvori");
        assertEquals("nádvoří", game.getGamePlan().getCurrentLocation().getName());
        assertEquals(false, game.isGameOver());
        game.processCommand("jdi mestecko");
        assertEquals("městečko", game.getGamePlan().getCurrentLocation().getName());
        assertEquals(false, game.isGameOver());
        game.processCommand("jdi hostinec");
        assertEquals("hostinec", game.getGamePlan().getCurrentLocation().getName());
        assertEquals(false, game.isGameOver());
        game.processCommand("seber brejle");
        assertEquals(false, game.isGameOver());
        game.processCommand("jdi mestecko");
        assertEquals("mestecko", game.getGamePlan().getCurrentLocation().getName());
        assertEquals(false, game.isGameOver());
        game.processCommand("otevri tajemstvi");
        assertEquals(false, game.isGameOver());
        game.processCommand("jdi tajemstvi");
        assertEquals("tajemstvi", game.getGamePlan().getCurrentLocation().getName());
        assertEquals(false, game.isGameOver());
        game.processCommand("otevri tajemstvi2");
        assertEquals(false, game.isGameOver());
        game.processCommand("jdi tajemstvi2");
        assertEquals("tajemstvi2", game.getGamePlan().getCurrentLocation().getName());
        assertEquals(false, game.isGameOver());
        game.processCommand("otevri tajemstvi3");
        assertEquals(false, game.isGameOver());
        game.processCommand("jdi tajemstvi3");
        assertEquals("tajemstvi3", game.getGamePlan().getCurrentLocation().getName());
        assertEquals(false, game.isGameOver());
        game.processCommand("jdi dalsi_pripad");
        assertEquals("dalsi_pripad", game.getGamePlan().getCurrentLocation().getName());
        assertEquals(true, game.isGameOver());
    }

    /***************************************************************************
     * Testuje, zda zavolání příkazu <b>konec</b> skutečně ukončí hru.
     */
    @Test
    public void testPlayerQuit() {
        assertEquals("merlinuv_domecek", game.getGamePlan().getCurrentLocation().getName());
        game.processCommand("jdi nadvori");
        assertEquals("nadvori", game.getGamePlan().getCurrentLocation().getName());
        assertEquals(false, game.isGameOver());
        game.processCommand("konec");
        assertEquals(true, game.isGameOver());
    }

}
