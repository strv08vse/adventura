package model;

/**
 * Toto je hlavní třída logiky aplikace. Třída vytváří instanci třídy GamePlan, která inicializuje lokace hry a vytváří seznam platných příkazů a instance tříd provádějící jednotlivé příkazy.
 * Vypisuje uvítací a ukončovací text hry. Také vyhodnocuje jednotlivé příkazy zadané uživatelem.
 * @author     Michael Kolling, Luboš Pavlíček, Jarmila Pavlíčková, Jan Říha @version    LS 2017/2018
 */
public class Game implements IGame
{
    /* Soubor je ulozen v kodovani UTF-8. Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy.*/
    private ListOfCommands listOfCommands;
    private GamePlan gamePlan;
    private boolean gameOver = false;

    /**
     * Vytváří hru a inicializuje lokace (prostřednictvím třídy GamePlan) a seznam platných příkazů.
     */
    public Game()
    {
        Inventory inventory = new Inventory();
        gamePlan = new GamePlan(inventory);
        
        listOfCommands = new ListOfCommands();
        listOfCommands.insertCommand(new CommandHelp(listOfCommands));
        listOfCommands.insertCommand(new CommandMove(gamePlan));
        listOfCommands.insertCommand(new CommandTerminate(this));
        
        listOfCommands.insertCommand(new CommandPick(gamePlan));
        listOfCommands.insertCommand(new CommandInventory(gamePlan));
        listOfCommands.insertCommand(new CommandDrop(gamePlan));
        listOfCommands.insertCommand(new CommandTalk(gamePlan));
        listOfCommands.insertCommand(new CommandUnlock(gamePlan, inventory));
        listOfCommands.insertCommand(new CommandExplore(gamePlan));

    }

    /**
     * Vrátí úvodní zprávu pro hráče.
     * @return úvodní zprávu pro hráče
     */
    public String getProlog()
    {
        return "Vitejte!\n" +
        "Toto je pribeh o detektivu Pikachu a jak mel skoncit svet.\n" +
        "Jste nejvetsi detektiv vsech dob, detektiv Pikachu, byl jste povolan k neodkladnemu pripadu.\n" +
        "Merlin byl nalezen mrtev ve své loznici a jeho kouzelný amulet, který chranil celou zemi před invazi vsemoznych entit, zmizel. Neni casu na zbyt.\n" +
        "Je potreba zjistit kdo je vrahem a ziskat amulet zpet.\n" +
        "\n" +
        "Napiste 'napoveda', pokud si nevite rady, jak hrat dal.\n" + "\n" + 
        gamePlan.getCurrentLocation().getFullDescription();
    }

    /**
     * Vrátí závěrečnou zprávu pro hráče.
     * @return závěrečnou zprávu pro hráče
     */
    public String getEpilog()
    {
        return "Dik, ze jste si zahrali. Ahoj.";
    }

    /**
     * Vrací true, pokud hra skončila.
     * @return true, pokud hra již skončila; jinak false
     */
    public boolean isGameOver()
    {
        return gameOver;
    }

    /**
     * Metoda zpracuje řetězec uvedený jako parametr, rozdělí ho na slovo příkazu a další parametry. Pak otestuje zda příkaz je klíčovým slovem, např. jdi. Pokud ano spustí samotné provádění příkazu.
     * @param line  text, který zadal uživatel jako příkaz do hry @return řetězec, který se má vypsat na obrazovku
     */
    @Override
    public String processCommand(String line)
    {
        String[] words = line.split("[ \t]+");
        String cmdWord = words[0];
        String[] parameters = new String[words.length - 1];

        for (int i = 0; i < parameters.length; i++) {
            parameters[i]= words[i+1];
        }

        String result = null;
        if (listOfCommands.checkCommand(cmdWord)) {
            ICommand command = listOfCommands.getCommand(cmdWord);
            result = command.process(parameters);
        } else {
            result = "Nevim, co tim myslis. Tento prikaz neznam.";
        }

        if (gamePlan.isVictorious()) {
            gameOver = true;
        }

        return result;
    }

    /**
     * Nastaví, že nastal konec hry, metodu využívá třída CommandTerminate, mohou ji použít i další implementace rozhraní ICommand.
     * @param gameOver příznak, zda hra již skončila
     */
    protected void setGameOver(boolean gameOver)
    {
        this.gameOver = gameOver;
    }

    /**
     * Metoda vrátí odkaz na herní plán, je využita hlavně v testech, kde se jejím prostřednictvím získává aktualní lokace hry.
     * @return herní plán
     */
    public GamePlan getGamePlan()
    {
        return gamePlan;
    }
}
