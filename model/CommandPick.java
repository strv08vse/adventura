package model;


/**
 * Třída <b>CommandPick</b> představuje příkaz pro zvednutí
 * předmětu.
 *
 * @author     Jan Říha, Vít Stříteský
 * @version    LS 2017/2018
 */
public class CommandPick implements ICommand
{
    private static final String NAME = "seber";
    
    private GamePlan plan;
    
    private Inventory inventory;
    
   /**
    * Konstruktor třídy.
    *
    * @param    plan herní plán, ve kterém se bude ve hře "chodit"
    */
    public CommandPick(GamePlan plan) {
        this.plan = plan;
        this.inventory = plan.getInventory();
    }
    
    /**
     * Metoda <b>process</b> představuje zpracování příkazu pro
     * sebrání předmětu. Metoda nejprve ověří, že byla zavolána
     * s jedním parametrem <i>(který představuje název předmětu)</i>.
     * Následně ověří, že daný předmět je v aktuální lokaci.
     * Následně ověří, že daný předmět je přenositelný. Následně
     * ověří, že je možné ho vložit do batohu <i>(že v batohu je
     * volné místo)</i>. Pokud jsou všechny podmínky splněné,
     * předmět je odebrán z lokace a umístěn do batohu.
     * 
     * @param  parameters  pole parametrů zadaných hráčem na příkazovou řádku
     * @return text informující o výsledku zpracování
     */
    public String process(String... parameters) {
        if (parameters.length < 1) {
            return "Nevim, co mam sebrat.";
        }
        
        if (parameters.length > 1) {
            return "Vice predmetu najednou sebrat nedokazu.";
        }
        
        String name = parameters[0];
        Location curLocation = plan.getCurrentLocation();
        
        if (!curLocation.containsItem(name)) {
            return "Predmet " + name + " tady neni.";
        }
        
        Item item = curLocation.removeItem(name);
        if (!item.isMoveable()) {
            curLocation.addItem(item);
            return "Predmet " + name + " fakt neuneses.";
        }

        
        if (inventory.isFull()) {
            curLocation.addItem(item);
            return "Inventar je plny";
        }
        
        inventory.addItem(item);
        

        return "Sebral jsi predmet " + name + ".";
    }

    /**
     * Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *
     * @return    název příkazu
     */
    public String getName() {
        return NAME;
    }
}
