package model;


public class Character
{
    private String name;
    private String speech;

    public Character(String name, String speech) {
        this.name = name;
        this.speech = speech;
    }

    public String getName() {
        return name; 
    }

    public String getSpeech() {
        return speech;
    }

    public void setSpeech(String newSpeech)
    {
        this.speech = newSpeech;
    }
}
