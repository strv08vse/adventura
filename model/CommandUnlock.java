package model;


/**
 * Třída CommandUnlock implementuje pro hru příkaz otevři.
 * Tato třída je součástí jednoduché textové hry.
 *
 * @author     Vít Stříteský
 * @version    LS 2017/2018
 */
public class CommandUnlock implements ICommand
{
    private static final String NAME = "otevri";
    private GamePlan plan;
    private Inventory inventory;

    /**
     * Konstruktor tridy CommandUnlock
     */
    public CommandUnlock(GamePlan plan, Inventory inventory){  
        this.plan = plan;
        this.inventory = inventory;
    }
    


    /**
     * Provádí příkaz "otevři". Zkouší se otevřít do zadaná lokace. Lokaci se
     * podařilo otevřít, pokud se nacházejí ve vedlejší locaci od aktuální a máme v
     * inventáři předmět potřebný k postupu dále.
     * 
     * @param parameters jako parametr obsahuje jméno lokace (východu), která se má odemknout.
     * 
     * @return zpráva, kterou vypíše hra hráči
     */

    public String process(String... parameters) {
        if (parameters.length == 0) {
            return "Co bych mel otevrit?";
        }

        String location = parameters[0];
        Location getExitLocation = plan.getCurrentLocation().getExitLocation(location);

        if (getExitLocation == null) {
            return "Odsud se nedostanes do "+location;
        }
        else {
            if (getExitLocation.isLocked()) {
                if (inventory.containsItem(getExitLocation.getKey().getName())) {
                    getExitLocation.setLocked(false);
                    inventory.dropItem(getExitLocation.getKey().getName());
                    return "Cesta je volna do " + location +" muzes pokracovat v patrani." ;

                }
                else {
                    return "Potrebujes spravnou indicii. ";
                }
            }
            else {
                return "Prostor "+location+" uz je otevreny.";
            }
        }
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return název příkazu
     */
    public String getName() {
        return NAME;
    }
}
