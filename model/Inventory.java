package model;

import java.util.*;


public class Inventory
{
    private Map<String, Item> items;
    private static final int KAPACITA = 4;

    public Inventory()
    {
        items = new HashMap<>();
    }

    public Item addItem(Item item){
            
        return items.put(item.getName(),item);

    }

    public boolean containsItem (String item) {
        for(String name : items.keySet()) {
            if(item.equals(name)){
                return true;
            }
        }
        return false;
    }

    public boolean dropItem(String item) {
        boolean droped = false;
        if (items.containsKey(item)) {
            droped = true;
            items.remove(item);

        }
        return droped;
    }

    public Item getItem(String name) {
        return items.get(name);
    }

    public boolean isFull() {
        return (items.size() >= KAPACITA);
    }

    public String getItems() {
        String text = "";
        for (String name : items.keySet()) {
            text +=  name + " ";
        }
        return text;
    }
}
