package model;



/**
 * Třída CommandInventory implementuje pro hru příkaz batoh.
 * Tato třída je součástí jednoduché textové hry.
 *
 * @author     Vít Stříteský
 * @version    LS 2017/2018
 */
public class CommandInventory implements ICommand
{
    private static final String NAME = "batoh";
    private GamePlan plan;

    public CommandInventory(GamePlan plan) {
        this.plan = plan;
    }
    
    /**
     * Jestli si hráč něco nese v inventáři, bude to vypsáno. V opačnšém případě se vypíše že je prazdný. 
     * 
     * @return    zpráva, kterou vypíše hra hráči
     */
    public String process(String... parameters) {
        if (plan.getInventory().getItems().equals("")) {
            return "V batohu nic nemas, je prazdny\n";
        }
        else {
            return "V batohu si neses: " + plan.getInventory().getItems() + "\n"; 
        }
    }
    
    /**
     * Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *
     * @return    název příkazu
     */
    public String getName() {
        return NAME;
    }
}
