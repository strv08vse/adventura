/* UTF-8 codepage: Příliš žluťoučký kůň úpěl ďábelské ódy. ÷ × ¤
 * «Stereotype», Section mark-§, Copyright-©, Alpha-α, Beta-β, Smile-☺
 */
package model;



import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.*;



/*******************************************************************************
 * Testovací třída {@code InventoryTest} slouží ke komplexnímu otestování
 * třídy {@link InventoryTest}.
 *
 * @author  Vít STříteský
 * @version LS 2017/2018
 */
public class InventoryTest
{
    private Item item1;
    private Inventory inventory1;
    
    /***************************************************************************
     * Inicializace předcházející spuštění každého testu a připravující tzv.
     * přípravek (fixture), což je sada objektů, s nimiž budou testy pracovat.
     */
    @Before
    public void setUp()
    {
        item1 = new Item("něco","něco");
        inventory1 = new Inventory();
    }


    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každého testu.
     */
    @After
    public void tearDown()
    {
        //vyčistění po testech
    }
    
    @Test
    public void testItemPick()
    {
        inventory1.addItem(item1);
        assertEquals(true, inventory1.containsItem("něco"));
        assertEquals(false, inventory1.containsItem("věc"));
    }
    
    @Test
    public void testItemDrop()
    {
        inventory1.addItem(item1);
        assertEquals(true, inventory1.containsItem("něco"));
        inventory1.dropItem("něco");
        assertEquals(false, inventory1.containsItem("něco"));
    }
}


