package model;


/**
 * Třída CommandExplore implementuje pro hru příkaz prohledej.
 * Tato třída je součástí jednoduché textové hry.
 *
 * @author     Vít Stříteský
 * @version    LS 2017/2018
 */
public class CommandExplore implements ICommand
{
    private static final String NAME = "prohledej";
    private GamePlan plan;
    
    /**
    * Konstruktor třídy.
    *
    * @param    plan herní plán, ve kterém se bude ve hře "chodit"
    */   
    public CommandExplore (GamePlan plan) {
        this.plan = plan;
    }

    /**
     *  Vypíše všechny informace o dané lokalitě
     *
     * @return    zpráva, kterou vypíše hra hráči
     */ 
    public String process(String... parameters) {
        return plan.getCurrentLocation().getFullDescription();
    } 

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    public String  getName() {
        return NAME;
    }
}
